<?php

namespace App\Providers;

use App\QueuePusher;
use Illuminate\Support\ServiceProvider;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class MyQueuePusherProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //$qp = new QueuePusher(new AMQPStreamConnection('localhost', 5672, 'guest', 'guest'));
        $this->app->singleton(QueuePusher::class, function () {
            //$conn = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            return new QueuePusher();
        });
    }
}
