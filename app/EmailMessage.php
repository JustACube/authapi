<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 28.07.18
 * Time: 20:10
 */

namespace App;


use App\Interfaces\IQueueMessage;
use JsonSerializable;

class EmailMessage implements JsonSerializable, IQueueMessage
{
    private $recipient;
    private $type;
    private $params;

    /**
     * EmailMessage constructor.
     * @param $recipient
     * @param $emailType
     */
    public function __construct(string $recipient,string $emailType)
    {
        $this->recipient = $recipient;
        $this->type = $emailType;
    }


    public function setRecipient(string $email)
    {
        $this->recipient = $email;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $array =[
            'recipient' => $this->recipient,
            'type' => $this->type,
            'params' => $this->params,
        ];
        return $array;
    }
}