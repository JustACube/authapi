<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordChange extends Mailable
{
    use Queueable, SerializesModels;

    protected $userName;
    protected $passChangeLink;

    /**
     * Create a new message instance.
     *
     * @param string $userName
     * @param string $passChangeLink
     */
    public function __construct(string $userName, string $passChangeLink)
    {
        $this->userName=$userName;
        $this->passChangeLink=$passChangeLink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.passwordChangeMail')->with([
            'userName' => $this->userName,
            'passChangeLink' => $this->passChangeLink,
        ]);
    }
}
