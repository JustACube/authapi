<?php

namespace App\Http\Controllers;

use App\EmailMessage;
use App\Http\Requests\CheckEmailRequest;
use App\Http\Requests\PassChangeRequest;
use App\QueuePusher;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordChangeController extends Controller
{
    public function initChanging(QueuePusher $queuePusher, CheckEmailRequest $request){
        $user = User::where('email',$request->email)->first();
        if ($user === null){
            return response()->json([
                'status' => 'The email doesnt exist',
            ]);
        }
        $data =  new DateTime();
        $user->remember_token = sha1($user->getEmail().$user->getFirstName().$user->getLastName().
                                $data->getTimestamp());
        $passChangeLink = 'auth.local/api/v1/passwordChange?token='.$user->remember_token;
        $user->update();
        $msg = new EmailMessage($user->getEmail(), 'passwordChange');
        $msg->setParams([
            'userName' => $user->getFirstName(),
            'passChangeLink' => $passChangeLink,
        ]);
        $queuePusher->publish($msg);
    }

    public function passChanging(PassChangeRequest $request){
        $user = User::where('remember_token',$request->token)->first();
        if ($user === null){
            return response()->json([
                'status' => 'Wrong Token',
            ]);
        }
        $user->changePassword($request->password);
        $user->update();
    }
}
