<?php

namespace App\Http\Controllers;

use App\EmailMessage;
use App\QueuePusher;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * @param QueuePusher $queuePusher
     * @param StoreUserRequest $request
     * @return array
     */
    public function register(QueuePusher $queuePusher, StoreUserRequest $request)
    {
        $user = new User();
        $user->setFirstName($request->firstName);
        $user->setLastName($request->lastName);
        $user->setEmail($request->email);
        $user->setPassword(Hash::make($request->password));
        $user->setCity($request->city);
        $user->save();

        $msg = new EmailMessage($user->getEmail(), 'welcome');
        $msg->setParams(['userName' => $user->getFirstName()]);
        $queuePusher->publish($msg);

        return fractal()
            ->item($user)
            ->transformWith(new UserTransformer())
            ->toArray();
    }
}
