<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\MySession;
use App\ProfileData;
use App\Transformers\ShowUserTransformer;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUserInfo(MySession $mySession, Request $request)
    {
        $token = $request->header('Authorization');
        if ($token!=null && $mySession->find($token)){
            $user = $mySession->getUser($token);
            return $user->getInfo();
        }

        return response()->json([
            'status' => 'Auth failed',
        ]);
    }
    public function updateUserInfo(MySession $mySession, UpdateUserRequest $request){
        $token = $request->header('Authorization');
        if ($token!=null && $mySession->find($token)) {
            $user = $mySession->getUser($token);
            $profileData = new ProfileData();
            $profileData->setFirstName($request->get('firstName', ''));
            $profileData->setLastName($request->get('lastName', ''));
            $profileData->setEmail($request->get('email', ''));
            $profileData->setCity($request->get('city', ''));
            $user->updateProfile($profileData);
            $user->save();
            return $user->getInfo();
        }
    }
}
