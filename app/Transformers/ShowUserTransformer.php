<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class ShowUserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'email' => $user->getEmail(),
            'city' => $user->getCity()
        ];
    }
}
